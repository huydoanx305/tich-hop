﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Net;
using System.Runtime.Serialization.Json;
using System.IO;

namespace DoanVanHuy_Winform
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            LoadDataGridView();
            LoadDonVi();  
        }
       

        private void LoadDataGridView()
        {
            string link = "http://localhost/hocrestful/api/nhanvien";
            HttpWebRequest request = WebRequest.CreateHttp(link);
            WebResponse response = request.GetResponse();
            DataContractJsonSerializer js = new DataContractJsonSerializer(typeof(NhanVien[]));
            object data = js.ReadObject(response.GetResponseStream());
            NhanVien[] arrNhanVien = data as NhanVien[];
            dataGridView1.DataSource = arrNhanVien;
        }

        private void LoadDonVi()
        {
            string link = "http://localhost/hocrestful/api/donvi";
            HttpWebRequest request = WebRequest.CreateHttp(link);
            WebResponse response = request.GetResponse();
            DataContractJsonSerializer js = new DataContractJsonSerializer(typeof(DonVi[]));
            object data = js.ReadObject(response.GetResponseStream());
            DonVi[] arrDonVi = data as DonVi[];

            cobDonVi.DataSource = arrDonVi;
            cobDonVi.ValueMember = "MaDonVi";
            cobDonVi.DisplayMember = "TenDonVi";
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            string postString = string.Format("?maNV={0}&hoTen={1}&gioiTinh={2}&hsl={3}&maDV={4}",
                txtId, txtHoTen, txtGioiTinh, txtHSL, cobDonVi.SelectedValue);
            string link = "http://localhost/hocrestful/api/nhanvien" + postString;

            HttpWebRequest request = WebRequest.CreateHttp(link);
            request.Method = "POST";
            request.ContentType = "application/json;charset=UTF-8";
            byte[] byteArray = Encoding.UTF8.GetBytes(postString);
            request.ContentLength = byteArray.Length;

            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            DataContractJsonSerializer js = new DataContractJsonSerializer(typeof(bool));
            object data = js.ReadObject(request.GetResponse().GetResponseStream());
            bool kq = (bool)data;
            if(kq)
            {
                LoadDataGridView();
                MessageBox.Show("Thêm nhân viên thành công");
            } else
            {
                MessageBox.Show("Thêm nhân viên thất bại");
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            string putString = string.Format("?maNV={0}&hoTen={1}&gioiTinh={2}&hsl={3}&maDV={4}",
                txtId, txtHoTen, txtGioiTinh, txtHSL, cobDonVi.SelectedValue);
            string link = "http://localhost/hocrestful/api/nhanvien" + putString;

            HttpWebRequest request = WebRequest.CreateHttp(link);
            request.Method = "PUT";
            request.ContentType = "application/json;charset=UTF-8";
            byte[] byteArray = Encoding.UTF8.GetBytes(putString);
            request.ContentLength = byteArray.Length;

            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            DataContractJsonSerializer js = new DataContractJsonSerializer(typeof(bool));
            object data = js.ReadObject(request.GetResponse().GetResponseStream());
            bool kq = (bool)data;
            if (kq)
            {
                LoadDataGridView();
                MessageBox.Show("Sửa nhân viên thành công");
            }
            else
            {
                MessageBox.Show("Sửa nhân viên thất bại");
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0) return;

            DataGridViewRow row = dataGridView1.SelectedRows[0];
            string maNV = row.Cells[0].Value.ToString();
            string deleteString = string.Format("?id={0}", maNV);
            string link = "http://localhost/hocrestful/api/nhanvien" + deleteString;

            HttpWebRequest request = WebRequest.CreateHttp(link);
            request.Method = "DELETE";
            request.ContentType = "application/json;charset=UTF-8";
            byte[] byteArray = Encoding.UTF8.GetBytes(deleteString);
            request.ContentLength = byteArray.Length;

            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            DataContractJsonSerializer js = new DataContractJsonSerializer(typeof(bool));
            object data = js.ReadObject(request.GetResponse().GetResponseStream());
            bool kq = (bool)data;
            if (kq)
            {
                LoadDataGridView();
                MessageBox.Show("Xóa nhân viên thành công");
            }
            else
            {
                MessageBox.Show("Xóa nhân viên thất bại");
            }
        }

        private void btnTim_Click(object sender, EventArgs e)
        {
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int d = e.RowIndex;
            txtId.Text = dataGridView1.Rows[d].Cells[0].Value.ToString();
            txtHoTen.Text = dataGridView1.Rows[d].Cells[1].Value.ToString();
            txtGioiTinh.Text = dataGridView1.Rows[d].Cells[2].Value.ToString();
            txtHSL.Text = dataGridView1.Rows[d].Cells[3].Value.ToString();
            cobDonVi.Text = dataGridView1.Rows[d].Cells[4].Value.ToString();
        }

    }
}
