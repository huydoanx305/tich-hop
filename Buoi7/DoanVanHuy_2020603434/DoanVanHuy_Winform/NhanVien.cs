﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoanVanHuy_Winform
{
    class NhanVien
    {
        public int Ma { get; set; }
        public string HoTen { get; set; }
        public string NgaySinh { get; set; }
        public string GioiTinh { get; set; }
        public double HSL { get; set; }
        public int MaDonVi { get; set; }

    }
}
