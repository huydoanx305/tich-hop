﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
    <xsl:output method="html" indent="yes"/>

    <xsl:template match="/DS">
		<html>
			<head>
				<title>Bài kiểm tra số 1</title>
				<link rel="stylesheet" type="text/css" href="Style.css"></link>
			</head>
			<body>
				<h1 align="center">BẢNG LƯƠNG THÁNG</h1>
				<xsl:apply-templates select="congty"/>
			</body>
		</html>
    </xsl:template>
	
	<xsl:template match="congty">
		<h2>Tên công ty: <xsl:value-of select="@TenCT"/></h2>
		<xsl:apply-templates select="donvi"/>
	</xsl:template>
	
	<xsl:template match="donvi">
		<h2>Tên phòng: <xsl:value-of select="tendv"/></h2>

		<table border="1" width="100%" cellspacing="0">
			<tr>
				<th>STT</th>
				<th>Họ tên</th>
				<th>Ngày sinh</th>
				<th>Giới tính</th>
				<th>Ngày công</th>
				<th>Lương</th>
			</tr>
			<xsl:apply-templates select="nhanvien"/>
		</table>
	</xsl:template>
	
	<xsl:template match="nhanvien">
		<xsl:variable name="ngayCong" select="ngaycong"></xsl:variable>
		<tr>
			<td><xsl:value-of select="position()"/></td>
			<td><xsl:value-of select="hoten"/></td>
			<td><xsl:value-of select="ngaysinh"/></td>
			<td><xsl:value-of select="gioitinh"/></td>
			<td><xsl:value-of select="$ngayCong"/></td>
			<xsl:choose>
				<xsl:when test="$ngayCong &lt;= 20">
					<td><xsl:value-of select="$ngayCong * 150000"/></td>
				</xsl:when>
				<xsl:when test="$ngayCong &lt;= 25">
					<td><xsl:value-of select="20 * 150000 + (($ngayCong - 20) * 200000)"/></td>
				</xsl:when>
				<xsl:otherwise>
					<td><xsl:value-of select="20 * 150000 + 5 * 200000 + (($ngayCong - 25) * 250000)"/></td>
				</xsl:otherwise>
			</xsl:choose>
		</tr>
	</xsl:template>
</xsl:stylesheet>
