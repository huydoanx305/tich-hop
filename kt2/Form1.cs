﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace DoanVanHuy_2020603434
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            combTrinhDo.Items.Add("Đại học");
            combTrinhDo.Items.Add("Cao đẳng");
            combTrinhDo.Items.Add("Trung cấp");
            combTrinhDo.SelectedIndex = 0;
            radioNam.Checked = true;
            HienThi();
        }

        XmlDocument doc = new XmlDocument();
        string fileName = @"C:\Users\ADMIN\OneDrive\Máy tính\tich-hop\kt2\DoanVanHuy_2020603434\DoanVanHuy_2020603434\dsnhanvien.xml";

        private void HienThi()
        {
            doc.Load(fileName);

            XmlNodeList ds = doc.SelectNodes("/ds/nhanvien");

            dataGridView1.Rows.Clear();
            dataGridView1.ColumnCount = 6;
            dataGridView1.Rows.Add();

            int sd = 0;
            foreach(XmlNode nv in ds)
            {
                XmlNode manv = nv.SelectSingleNode("@manv");
                XmlNode ho = nv.SelectSingleNode("hoten/ho");
                XmlNode ten = nv.SelectSingleNode("hoten/ten");
                XmlNode gioitinh = nv.SelectSingleNode("gioitinh");
                XmlNode trinhdo = nv.SelectSingleNode("trinhdo");
                XmlNode diachi = nv.SelectSingleNode("diachi");

                dataGridView1.Rows[sd].Cells[0].Value = manv.InnerText.ToString();
                dataGridView1.Rows[sd].Cells[1].Value = ho.InnerText.ToString();
                dataGridView1.Rows[sd].Cells[2].Value = ten.InnerText.ToString();
                dataGridView1.Rows[sd].Cells[3].Value = gioitinh.InnerText.ToString();
                dataGridView1.Rows[sd].Cells[4].Value = trinhdo.InnerText.ToString();
                dataGridView1.Rows[sd].Cells[5].Value = diachi.InnerText.ToString();

                dataGridView1.Rows.Add();
                sd++;
            }
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            doc.Load(fileName);

            XmlElement root = doc.DocumentElement;

            XmlNode timnhanvien = doc.SelectSingleNode("/ds/nhanvien[@manv='" + txtMa.Text + "']");
            if(timnhanvien != null)
            {
                MessageBox.Show("Mã nhân viên đã tồn tại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            XmlNode nhanvien = doc.CreateElement("nhanvien");

            XmlAttribute manv = doc.CreateAttribute("manv");
            manv.InnerText = txtMa.Text;
            nhanvien.Attributes.Append(manv);


            XmlNode hoten = doc.CreateElement("hoten");

            XmlNode ho = doc.CreateElement("ho");
            ho.InnerText = txtHo.Text;
            hoten.AppendChild(ho);

            XmlNode ten = doc.CreateElement("ten");
            ten.InnerText = txtTen.Text;
            hoten.AppendChild(ten);

            nhanvien.AppendChild(hoten);


            XmlNode gioitinh = doc.CreateElement("gioitinh");
            if (radioNam.Checked) gioitinh.InnerText = "Nam";
            if (radiaNu.Checked) gioitinh.InnerText = "Nữ";
            nhanvien.AppendChild(gioitinh);

            XmlNode trinhdo = doc.CreateElement("trinhdo");
            trinhdo.InnerText = combTrinhDo.SelectedItem.ToString();
            nhanvien.AppendChild(trinhdo);

            XmlNode diachi = doc.CreateElement("diachi");
            diachi.InnerText = txtDiaChi.Text;
            nhanvien.AppendChild(diachi);

            root.AppendChild(nhanvien);

            doc.Save(fileName);
            HienThi();
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            doc.Load(fileName);

            XmlElement root = doc.DocumentElement;

            XmlNode nhanviencu = doc.SelectSingleNode("/ds/nhanvien[@manv='" + txtMa.Text + "']");
            if(nhanviencu == null)
            {
                MessageBox.Show("Nhân viên không tồn tại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            XmlNode nhanvienmoi = doc.CreateElement("nhanvien");

            XmlAttribute manv = doc.CreateAttribute("manv");
            manv.InnerText = txtMa.Text;
            nhanvienmoi.Attributes.Append(manv);


            XmlNode hoten = doc.CreateElement("hoten");

            XmlNode ho = doc.CreateElement("ho");
            ho.InnerText = txtHo.Text;
            hoten.AppendChild(ho);

            XmlNode ten = doc.CreateElement("ten");
            ten.InnerText = txtTen.Text;
            hoten.AppendChild(ten);

            nhanvienmoi.AppendChild(hoten);


            XmlNode gioitinh = doc.CreateElement("gioitinh");
            if (radioNam.Checked) gioitinh.InnerText = "Nam";
            if (radiaNu.Checked) gioitinh.InnerText = "Nữ";
            nhanvienmoi.AppendChild(gioitinh);

            XmlNode trinhdo = doc.CreateElement("trinhdo");
            trinhdo.InnerText = combTrinhDo.SelectedItem.ToString();
            nhanvienmoi.AppendChild(trinhdo);

            XmlNode diachi = doc.CreateElement("diachi");
            diachi.InnerText = txtDiaChi.Text;
            nhanvienmoi.AppendChild(diachi);

            root.ReplaceChild(nhanvienmoi, nhanviencu);

            doc.Save(fileName);
            HienThi();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            DialogResult d = MessageBox.Show("Bạn có chắc chắn muốn xóa nhân viên này?", "Thông báo", 
                MessageBoxButtons.YesNo, MessageBoxIcon.Error);

            if(d == DialogResult.Yes)
            {
                doc.Load(fileName);

                XmlElement root = doc.DocumentElement;

                XmlNode nhanvienxoa = root.SelectSingleNode("/ds/nhanvien[@manv='" + txtMa.Text + "']");

                root.RemoveChild(nhanvienxoa);
                doc.Save(fileName);
                HienThi();
            }
        }

        private void btnTim_Click(object sender, EventArgs e)
        {
            doc.Load(fileName);

            dataGridView1.Rows.Clear();
            dataGridView1.ColumnCount = 6;
            dataGridView1.Rows.Add();
            int sd = 0;

            string keyword = radioNam.Checked ? "Nam" : "Nữ";
            XmlNodeList ds = doc.SelectNodes("/ds/nhanvien[@manv>'" + 2 + "'and@manv<'" + 4 + "']");
            foreach(XmlNode nv in ds)
            {
                XmlNode manv = nv.SelectSingleNode("@manv");
                XmlNode ho = nv.SelectSingleNode("hoten/ho");
                XmlNode ten = nv.SelectSingleNode("hoten/ten");
                XmlNode gioitinh = nv.SelectSingleNode("gioitinh");
                XmlNode trinhdo = nv.SelectSingleNode("trinhdo");
                XmlNode diachi = nv.SelectSingleNode("diachi");
                
                dataGridView1.Rows[sd].Cells[0].Value = manv.InnerText.ToString();
                dataGridView1.Rows[sd].Cells[1].Value = ho.InnerText.ToString();
                dataGridView1.Rows[sd].Cells[2].Value = ten.InnerText.ToString();
                dataGridView1.Rows[sd].Cells[3].Value = gioitinh.InnerText.ToString();
                dataGridView1.Rows[sd].Cells[4].Value = trinhdo.InnerText.ToString();
                dataGridView1.Rows[sd].Cells[5].Value = diachi.InnerText.ToString();
                dataGridView1.Rows.Add();
                sd++;
            } 
        }

        private void btnPreview_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("Explorer.exe", fileName);
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int d = e.RowIndex;
            txtMa.Text = dataGridView1.Rows[d].Cells[0].Value.ToString();
            txtHo.Text = dataGridView1.Rows[d].Cells[1].Value.ToString();
            txtTen.Text = dataGridView1.Rows[d].Cells[2].Value.ToString();

            if(dataGridView1.Rows[d].Cells[3].Value.ToString() == "Nam")
            {
                radioNam.Checked = true;
            } else
            {
                radiaNu.Checked = true;
            }
            
            combTrinhDo.SelectedItem = dataGridView1.Rows[d].Cells[4].Value.ToString();

            txtDiaChi.Text = dataGridView1.Rows[d].Cells[5].Value.ToString();
        }

    }
}
