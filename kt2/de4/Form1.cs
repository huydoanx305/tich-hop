﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            HienThi();
        }

        XmlDocument doc = new XmlDocument();
        string fileName = @"C:\Users\ADMIN\OneDrive\Máy tính\tich-hop\ontap\WindowsFormsApp1\WindowsFormsApp1\lophoc.xml";

        private void HienThi()
        {
            doc.Load(fileName);

            XmlNodeList lophoc = doc.SelectNodes("/lophoc/sinhvien");

            dataGridView1.Rows.Clear();
            dataGridView1.ColumnCount = 6;
            dataGridView1.Rows.Add();

            int i = 0;
            foreach(XmlNode sv in lophoc)
            {
                XmlNode masv = sv.SelectSingleNode("@masv");
                XmlNode tuoi = sv.SelectSingleNode("@tuoi");
                XmlNode hoten = sv.SelectSingleNode("hoten");
                XmlNode diachi = sv.SelectSingleNode("diachi");
                XmlNode tenmon = sv.SelectSingleNode("monhoc/tenmon");
                XmlNode diem = sv.SelectSingleNode("monhoc/diem");

                dataGridView1.Rows[i].Cells[0].Value = masv.InnerText.ToString();
                dataGridView1.Rows[i].Cells[1].Value = hoten.InnerText.ToString();
                dataGridView1.Rows[i].Cells[2].Value = tuoi.InnerText.ToString();
                dataGridView1.Rows[i].Cells[3].Value = diachi.InnerText.ToString();
                dataGridView1.Rows[i].Cells[4].Value = tenmon.InnerText.ToString();
                dataGridView1.Rows[i].Cells[5].Value = diem.InnerText.ToString();
                dataGridView1.Rows.Add();
                i++;
            }
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            doc.Load(fileName);

            XmlNode timsinhvien = doc.SelectSingleNode("/lophoc/sinhvien[@masv='" + txtMa.Text + "']");
            if(timsinhvien != null)
            {
                MessageBox.Show("Mã sinh viên đã tồn tại!", "Thông báo",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            XmlElement root = doc.DocumentElement;

            XmlNode sinhvien = doc.CreateElement("sinhvien");

            XmlAttribute masv = doc.CreateAttribute("masv");
            masv.InnerText = txtMa.Text;
            sinhvien.Attributes.Append(masv);

            XmlAttribute tuoi = doc.CreateAttribute("tuoi");
            tuoi.InnerText = txtTuoi.Text;
            sinhvien.Attributes.Append(tuoi);

            XmlNode hoten = doc.CreateElement("hoten");
            hoten.InnerText = txtHoTen.Text;
            sinhvien.AppendChild(hoten);

            XmlNode diachi = doc.CreateElement("diachi");
            diachi.InnerText = txtDiaChi.Text;
            sinhvien.AppendChild(diachi);

            XmlNode monhoc = doc.CreateElement("monhoc");

            XmlNode tenmon = doc.CreateElement("tenmon");
            tenmon.InnerText = txtTenMon.Text;
            monhoc.AppendChild(tenmon);

            XmlNode diem = doc.CreateElement("diem");
            diem.InnerText = txtDiem.Text;
            monhoc.AppendChild(diem);

            sinhvien.AppendChild(monhoc);

            root.AppendChild(sinhvien);
            doc.Save(fileName);
            HienThi();
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            doc.Load(fileName);

            XmlElement root = doc.DocumentElement;

            XmlNode sinhviencu = doc.SelectSingleNode("/lophoc/sinhvien[@masv='" + txtMa.Text + "']");
            if(sinhviencu == null)
            {
                MessageBox.Show("Sinh viên không tồn tại!", "Thông báo",
                   MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            XmlNode sinhvien = doc.CreateElement("sinhvien");

            XmlAttribute masv = doc.CreateAttribute("masv");
            masv.InnerText = txtMa.Text;
            sinhvien.Attributes.Append(masv);

            XmlAttribute tuoi = doc.CreateAttribute("tuoi");
            tuoi.InnerText = txtTuoi.Text;
            sinhvien.Attributes.Append(tuoi);

            XmlNode hoten = doc.CreateElement("hoten");
            hoten.InnerText = txtHoTen.Text;
            sinhvien.AppendChild(hoten);

            XmlNode diachi = doc.CreateElement("diachi");
            diachi.InnerText = txtDiaChi.Text;
            sinhvien.AppendChild(diachi);

            XmlNode monhoc = doc.CreateElement("monhoc");

            XmlNode tenmon = doc.CreateElement("tenmon");
            tenmon.InnerText = txtTenMon.Text;
            monhoc.AppendChild(tenmon);

            XmlNode diem = doc.CreateElement("diem");
            diem.InnerText = txtDiem.Text;
            monhoc.AppendChild(diem);

            sinhvien.AppendChild(monhoc);

            root.ReplaceChild(sinhvien, sinhviencu);
            doc.Save(fileName);
            HienThi();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            DialogResult d = MessageBox.Show("Bạn có chắn chắn muốn xóa sinh viên này?", "Thông báo",
                MessageBoxButtons.YesNo, MessageBoxIcon.Error);

            if(d == DialogResult.No)
            {
                return;
            }

            doc.Load(fileName);

            XmlElement root = doc.DocumentElement;

            XmlNode sinhvienxoa = doc.SelectSingleNode("/lophoc/sinhvien[@masv='" + txtMa.Text + "']");

            if(sinhvienxoa == null)
            {
                MessageBox.Show("Sinh viên không tồn tại!", "Thông báo",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            root.RemoveChild(sinhvienxoa);
            doc.Save(fileName);
            HienThi();
        }

        private void btnTim_Click(object sender, EventArgs e)
        {
            doc.Load(fileName);

            XmlNodeList lophoc = doc.SelectNodes("/lophoc/sinhvien[@tuoi>='" 
                + txtFrom.Text + "' and @tuoi<='" + txtTo.Text + "']");

            if (lophoc == null || lophoc.Count == 0)
            {
                MessageBox.Show("Không có sinh viên nào thuộc độ tuổi này!", "Thông báo",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            dataGridView1.Rows.Clear();
            dataGridView1.ColumnCount = 6;
            dataGridView1.Rows.Add();

            int i = 0;
            foreach (XmlNode sv in lophoc)
            {
                XmlNode masv = sv.SelectSingleNode("@masv");
                XmlNode tuoi = sv.SelectSingleNode("@tuoi");
                XmlNode hoten = sv.SelectSingleNode("hoten");
                XmlNode diachi = sv.SelectSingleNode("diachi");
                XmlNode tenmon = sv.SelectSingleNode("monhoc/tenmon");
                XmlNode diem = sv.SelectSingleNode("monhoc/diem");

                dataGridView1.Rows[i].Cells[0].Value = masv.InnerText.ToString();
                dataGridView1.Rows[i].Cells[1].Value = hoten.InnerText.ToString();
                dataGridView1.Rows[i].Cells[2].Value = tuoi.InnerText.ToString();
                dataGridView1.Rows[i].Cells[3].Value = diachi.InnerText.ToString();
                dataGridView1.Rows[i].Cells[4].Value = tenmon.InnerText.ToString();
                dataGridView1.Rows[i].Cells[5].Value = diem.InnerText.ToString();
                dataGridView1.Rows.Add();
                i++;
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int i = e.RowIndex;
            txtMa.Text = dataGridView1.Rows[i].Cells[0].Value.ToString();
            txtHoTen.Text = dataGridView1.Rows[i].Cells[1].Value.ToString();
            txtTuoi.Text = dataGridView1.Rows[i].Cells[2].Value.ToString();
            txtDiaChi.Text = dataGridView1.Rows[i].Cells[3].Value.ToString();
            txtTenMon.Text = dataGridView1.Rows[i].Cells[4].Value.ToString();
            txtDiem.Text = dataGridView1.Rows[i].Cells[5].Value.ToString();
        }
    }
}
